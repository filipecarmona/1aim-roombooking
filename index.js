angular.module('RoomApp', ['ngMaterial', 'ngMessages', 'ngMdIcons', 'ngResource','angularMoment']);

angular.module('RoomApp').controller('MainCtrl', MainCtrl);
MainCtrl.$inject = ['$scope'];
function MainCtrl($scope){
	$scope.toggleLeft = buildDelayedToggler('left');
    function debounce(func, wait, context) {
      var timer;
      return function debounced() {
        var context = $scope,
            args = Array.prototype.slice.call(arguments);
        $timeout.cancel(timer);
        timer = $timeout(function() {
          timer = undefined;
          func.apply(context, args);
        }, wait || 10);
      };
    }
    function buildDelayedToggler(navID) {
      return debounce(function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }, 200);
    }
    function buildToggler(navID) {
      return function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }
    }
}//MainCtrl

angular.module('RoomApp').controller('LeftCtrl', MainCtrl);
MainCtrl.$inject = ["$scope", "$timeout", "$mdSidenav", "$log"];
function LeftCtrl($scope, $timeout, $mdSidenav, $log){
    $scope.close = function () {
      $mdSidenav('left').close()
        .then(function () {
          $log.debug("close LEFT is done");
        });
    };
}//LeftCtrl

angular.module('RoomApp').run(AppRun);
AppRun.$inject = ['$rootScope','ApiService','moment'];
function AppRun($rootScope, ApiService, moment) {
	$rootScope.aimApi = {};
	$rootScope.aimApi.baseUrl = 'https://challenges.1aim.com/roombooking/';
	$rootScope.dateParam = new Date();
	$rootScope.moment = moment;

	$rootScope.$watch('dateParam', function (newVal) {
		ApiService.getRooms(newVal);
	})
}//AppRun

angular.module('RoomApp').controller('ScheduleCtrl', ScheduleCtrl);
ScheduleCtrl.$inject = ['ApiService','moment'];
function ScheduleCtrl(ApiService, moment) {
	var thisCtrl = this;

	thisCtrl.schedulingData = {
		date: $rootScope.dateParam,

	};

}//ScheduleCtrl

angular.module('RoomApp').service('ApiService',ApiService);
ApiService.$inject = ['$rootScope'];
function ApiService($rootScope) {
	var thisSvc = this;
	var baseUrl = "https://challenges.1aim.com/roombooking";
	

	thisSvc.getRooms = function getRooms(date) {
		var payloadObject = {date: $rootScope.moment(date).unix()};

		var data = JSON.stringify(payloadObject);
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("readystatechange", function () {
		  if (this.readyState === 4) {
		  	console.log("/getrooms response");
		    console.dir(this.responseText);
		  }
		});
		xhr.open("POST", baseUrl+"/getrooms");
		xhr.send(data);

	};

	thisSvc.sendPass = function sendPass(dateTimeStart, dateTimeEnd, people) {
		//xhr into sendpass
		
	};
}//ApiService

/* example object for sendpass api:
{
	//information about the booking
	booking: { 
		date: /UNIX Timestamp/ | "now" | "today",
		//only the hour and minute of the time_ timestamps are used, 
		//the day is always determined by date
		time_start: /UNIX Timestamp/,
		time_end: /UNIX Timestamp/,
		title: "event title",
		description: "event description"
		room: "name of the room that is beeing booked"
	 },
	 //information about all atendees
	 //all 3 fields are required
	 passes: [
		{ name: "Any Name",
		  email: "email@example.com",
		  number: "valid phone number"
		},
		...
	 ]
}*/